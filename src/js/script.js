(($)=>{	  
  const initSlider = ()=>{ 
    $('.slider').slick({
      autoplay: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      speed: 1200,
      arrows: false
    });
  },

	initGallery = ()=>{
		$('.popup-gallery').magnificPopup({
	    delegate: 'a',
	    type: 'image',
	    gallery: {
	      enabled: true
	    }
	  }); 		
	}

	initModalClose=()=>{
		const modal = $('.modal');
		$('.close-modal').on('click',(e)=>{
			modal.removeClass('is-active');
		});
	}

	initModalAboutAndLegals=()=>{
  	let modalLauncherMenu = $('.nav-main  .modal-menu a');
  	modalLauncherMenu.on('click',(e)=>{
			e.preventDefault();
			const modalName = e.currentTarget.getAttribute( 'title' ), 
						modal		  = $('.modal.legals')[0];
			openModal(modal);
  	});
  }
  
  initModalTags=()=>{
  	$('.tag-link').on('click', (e)=>{
			const tag     = e.target.attributes['data-tag'].value;
			const tagName = e.target.attributes['data-tag-name'].value;
			getProjetsByTag(tag, tagName);
		});
  }
	
	//GESTION TAGS
	getProjetsByTag = (tag, tagName)=>{
		$.post(
	    abri.ajaxUrl,{ 'action': 'get_projets_by_tag','tag':tag,},
	    (resp)=>{
	  		appendLinks(resp,tagName );
	    },
	    dataType = 'json'
		);	
	}
	
	appendLinks=(resp, tagName)=>{
		let modal = $('.modal-tags')[0];
	  let links =`<h2>#${tagName}<h3>`;
    resp.forEach((x)=>{
    	links+=x;
    })
    
    $('#modal-root').empty();
    $('#modal-root').append(links);
    openModal(modal);
	}

 	openModal=(modal)=>{
  	modal.classList.toggle('is-active');
    initModalClose();
  }
   
  scrollBackground = ()=>{
  	$( window ).scroll((e)=>{
  		const scroll = window.scrollY/30;
  		const backgroundSize= (100 - scroll) +"vw";
  		$('body').css( "background-size", backgroundSize )
		});
  }
	
	$(document).ready(function () {
		initSlider();
		initGallery()
		initModalClose();
		initModalTags();
		initModalAboutAndLegals();
		scrollBackground();
  }); 
	

})( jQuery );