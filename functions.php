<?php
/**
 * Timber starter-theme
 * https://github.com/timber/starter-theme
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

require_once('inc/admin_util.php');
require_once('inc/acf_field_import.php');
require_once('inc/ajax.php');

$composer_autoload = __DIR__ . '/vendor/autoload.php';
if ( file_exists($composer_autoload) ) {
	require_once( $composer_autoload );
	$timber = new Timber\Timber();
}

if ( ! class_exists( 'Timber' ) ) {

	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	});

	add_filter('template_include', function( $template ) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});
	return;
}


Timber::$dirname = array( 'templates', 'views' );
Timber::$autoescape = false;

class StarterSite extends Timber\Site {
	/** Add timber support. */
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
		add_filter( 'timber/context', array( $this, 'add_to_context' ) );
		add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		parent::__construct();
	}
	/** This is where you can register custom post types. */
	public function register_post_types() {
		// Register projet abri
		$labels = array(
			'name'                  => 'Projets',
			'singular_name'         => 'Projet',
			'menu_name'             => 'Projets',
			'name_admin_bar'        => 'Projets',
			'archives'              => 'Projets',
			'attributes'            => 'attributs projets',
			'parent_item_colon'     => 'Parent Item:',
			'all_items'             => 'Tous les projets',
			'add_new_item'          => 'Créer projet',
			'add_new'               => 'Créer projet',
			'new_item'              => 'nouveau projet',
			'edit_item'             => 'éditer projet',
			'update_item'           => 'modifier projet',
			'view_item'             => 'voir projet',
			'view_items'            => 'voir les projets',
			'search_items'          => 'chercher projets',
			'not_found'             => 'Aucun projet trouvé',
			'not_found_in_trash'    => 'Aucun projet trouvé dans la borbeille',
			'featured_image'        => 'vignette projet',
			'set_featured_image'    => 'choisir vignette',
			'remove_featured_image' => 'supprimer vignette',
			'use_featured_image'    => 'utiliser comme vignette',
			'insert_into_item'      => 'insérer dans projet',
			'uploaded_to_this_item' => 'dans ce projet',
			'items_list'            => 'listes des projets',
			'items_list_navigation' => 'listes des projets',
			'filter_items_list'     => 'filtrer listes des projets',
		);
		$args = array(
			'label'                 => 'Projet',
			'description'           => 'Les projets abri',
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'thumbnail' ),
			'taxonomies'            => array( 'post_tag' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-images-alt2',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
		);
		register_post_type( 'projet', $args );
	}
	
	public function add_to_context( $context ) {
		$context['menu']       = new Timber\Menu('main');
		$context['site']       = $this;
		$context['background'] = $this->select_background();
		
		return $context;
	}

	public function select_background (){
		$background_svg = array_values(array_filter(scandir(get_stylesheet_directory()."/img"), 
			function($item){
    		return $item[0] !== '.';
			}
		));
		$compteur_semaine = date("W")%4;
		$background_svg_chosen = $background_svg[$compteur_semaine];
		return $background_svg_chosen;
	}

	public function theme_supports() {
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'post-formats', array('status' ) );
	}

	public function add_to_twig( $twig ) {
		$twig->addExtension( new Twig_Extension_StringLoader() );

		$twig->addFunction( new Timber\Twig_Function( 'is_singular', 'is_singular' ) );

		$twig->addFilter( new Twig_SimpleFilter( 'myfoo', array( $this, 'myfoo' ) ) );
		return $twig;
	}


}
new StarterSite();

function abri_enqueue_style(){
 
 	wp_enqueue_script('jquery');
	wp_enqueue_script( 'magnific', get_stylesheet_directory_uri().'/dist/js/magnific.min.js', array('jquery'), true, true );
	wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js', array('jquery'), true, true );
	
	wp_enqueue_script( 'script', get_stylesheet_directory_uri().'/dist/js/script.js', array('jquery', 'slick', 'magnific'), true, true );

	wp_enqueue_style(  'abri-style', get_stylesheet_directory_uri().'/dist/css/style.css');	

	$variable_main_script = array(
		'ajaxUrl' => admin_url( 'admin-ajax.php' ),
		'a_value' => '10'
	);
	wp_localize_script( 'script', 'abri', $variable_main_script );

}
add_action( 'wp_enqueue_scripts', 'abri_enqueue_style');



function wpcf_filter_terms_order( $orderby, $query_vars, $taxonomies ) {
    return $query_vars['orderby'] == 'rand' ? 'rand()' : $orderby;
	
}

add_filter( 'get_terms_orderby', 'wpcf_filter_terms_order', 10, 3 );
