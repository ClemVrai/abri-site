<?php 
add_action( 'wp_ajax_nopriv_get_projets_by_tag', 'get_projets_by_tag_handler' );
add_action( 'wp_ajax_get_projets_by_tag', 'get_projets_by_tag_handler' );

function get_projets_by_tag_handler(){
	$tag_id = intval ($_POST['tag']);
	$args = array(
		'post_type' => 'projet', 
		'tag__in'   => $tag_id,
		//'fields'    =>'ids',
	);
	$posts = get_posts( $args );
	$posts_to_send=array();
	foreach ($posts as $key=>$post) {
		$id = $post->ID;
		$post_html = "<a class=\"post-links\" href=".$post->guid."><strong>".$post->post_title."</strong> : <em>".get_field("nom_oeuvre",$id);
		$post_html .= "</em> by ". get_field("artiste",$id)."</a>";
		$posts[$key]=$post_html;
	}
	echo json_encode($posts);
	wp_die();
}