<?php 
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title'	=> 'Options',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

//Clean head 
add_action( 'init', 'clean_head' );
function clean_head() {
	//DISABLE EMOJIS	
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	//DISABLE FEEDS
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'rest_output_link_wp_head', 10);
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10);
	remove_action( 'template_redirect', 'rest_output_link_header', 11, 0);
	remove_action( 'wp_head', 'rsd_link');
	remove_action( 'wp_head', 'wlwmanifest_link');
	remove_action( 'wp_head', 'wp_shortlink_wp_head');
	//Remove version 
	add_filter('the_generator','__return_false');
	//Remove xmlrpc
	add_filter('xmlrpc_enabled', '__return_false');
}

//Remove block library 
add_action( 'wp_enqueue_scripts', 'remove_block_library', 100 );	
function remove_block_library() { wp_dequeue_style( 'wp_block_library' );}


//Add admin columns
add_filter( 'manage_projet_posts_columns', 'projet_columns_head_only', 10);
add_filter( 'manage_edit-projet_sortable_columns', 'projet_columns_order_only');
add_action( 'manage_projet_posts_custom_column', 'projet_columns_body_only', 10, 2);
add_action( 'pre_get_posts', 'projet_column_orderby' );

function projet_columns_head_only($col) {
   $col = array(
			'cb'            => $col['cb'],
			'title'         => __( 'Title' ),
			'nom_projet'    => 'Nom œuvre',
			'nature_projet' => 'Nature projet',  
			'couv'          => 'couverture',
			'tags'          => __( 'Tags' ),
			'date'          => __( 'Date' ),
    );
   return $col;
}

function projet_columns_body_only($column_name, $post_ID) {
    if ($column_name == 'nom_projet') :
    	echo get_field('nom_oeuvre',$post_ID);
    
    elseif ($column_name == 'nature_projet'):
    	echo get_field('nature_projet',$post_ID);

    elseif ($column_name == 'couv'):
    	echo get_the_post_thumbnail( $post_ID, array(80, 80) );
    endif;
}

function projet_columns_order_only($col){
	$col['nom_projet']    = 'nom_projet';
	$col['nature_projet'] = 'nature_projet';
  return $col;
}

function projet_column_orderby( $query ) {

  $orderby = $query->get( 'orderby');

  if( 'nom_projet' == $orderby ) :
      $query->set('meta_key','nom_oeuvre');
      $query->set('orderby','meta_value');
  elseif('nature_projet'== $orderby):
		$query->set('meta_key','nature_projet');
    $query->set('orderby','meta_value');
  endif;
}