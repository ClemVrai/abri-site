<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */


$args_projets = array(
	'post_type'      => 'projet',
	'posts_per_page' =>50,
);

$args_actus = array(
	'post_type'      => 'post',
	'posts_per_page' =>15,
	'tax_query' => array(
		array(
      'taxonomy' => 'post_format',
      'field'    => 'slug',
			'terms'    => array( 'post-format-status' ),
			'operator' =>'NOT IN'
		),
	)
);

$args_tags = array(
	'orderby'    =>'rand',
	'taxonomy'   => 'post_tag',
	'hide_empty' => true,
	'number'     =>'30',
);


$arg_flash = $args_actus;
$arg_flash['tax_query'][0]['operator'] = 'IN';

$context            = Timber::context();

$context['slides']  = get_field('slider_home','options');
$context['video']   = get_field('video_home','options');

$context['projets'] = new Timber\PostQuery($args_projets);
$context['actus']   = new Timber\PostQuery($args_actus);
$context['flashs']  = new Timber\PostQuery($arg_flash);
$context['tags']    = get_terms($args_tags);

Timber::render( 'home.twig', $context );